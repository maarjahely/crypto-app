import React, {Fragment} from 'react';
import Table from './Components/Table'
import Form from './Components/Form'

class AddCrypto extends React.Component {
    constructor(props) {
          super(props)
          this.state = {
             cryptos: [],
          }
    }

    componentDidMount() {
        this.fetchData();
    }

    reloadData = () => {
        this.fetchData();
        console.log("Reloading data");
    }

    fetchData() {
        fetch('https://damp-cliffs-96306.herokuapp.com/crypto')
            .then(response => {
                 if (response.ok) {
                   return response.json();
                 } else {
                   throw new Error('Something went wrong ...');
                 }
               })
            .then(data => this.setState({ cryptos: data }));
    }

   render() {
      const {cryptos} = this.state;

      return (
         <Fragment>
            <Form reloadData={this.reloadData}/>
            <Table cryptos={cryptos} reloadData={this.reloadData}/>
         </Fragment>
      )
   }
}

export default AddCrypto;