import React, {Fragment} from 'react';

class Table extends React.Component {

   renderTableHeader() {
        return <Fragment>
                    <th>Cryptocurrency</th>
                    <th>Amount</th>
                    <th>Date of purchase</th>
                    <th>Wallet location</th>
                    <th>Current market value (EUR)</th>
                    <th></th>
                </Fragment>;
   }

   deleteCrypto = (id) => {
        const requestOptions = {
           method: 'DELETE'
        };
        fetch('https://damp-cliffs-96306.herokuapp.com/crypto/' + id, requestOptions)
            .then(result => this.props.reloadData())
   }

   renderTableData() {
      const {cryptos} = this.props;

      return cryptos.map((crypto, index) => {
         const { id, cryptoType, amount, purchaseDate, walletLocation, marketValue } = crypto
         return (
            <tr key={id}>
               <td>{cryptoType}</td>
               <td>{amount}</td>
               <td>{purchaseDate}</td>
               <td>{walletLocation}</td>
               <td>{marketValue}</td>
               <td onClick={() => {if (window.confirm('Are you sure you wish to delete this item?')) this.deleteCrypto(id)}}>
                <button className='btn btn-danger'>Delete</button>
               </td>
            </tr>
         )
      })
   }

   render() {

      return (
         <div style={{
                      backgroundColor: 'lightgrey'
                    }}>
         <hr/>
            <h3 id='title'>Crypto Portfolio</h3>
            <table id='cryptos' className='table'>
               <tbody>
                  <tr>{this.renderTableHeader()}</tr>
                  {this.renderTableData()}
               </tbody>
            </table>
         </div>
      )
   }
}


export default Table;