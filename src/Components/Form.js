import React from 'react';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          cryptoType: 'BITCOIN',
          amount: null,
          purchaseDate: '',
          walletLocation: ''
        };
    }

    componentDidMount() {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate());
        var date = currentDate.toISOString().substr(0,10);
        this.setState({purchaseDate: date})
    }

    mySubmitHandler = (event) => {
        event.preventDefault();

        const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    amount: this.state.amount,
                    cryptoType: this.state.cryptoType,
                    purchaseDate: this.state.purchaseDate,
                    walletLocation: this.state.walletLocation
                 })
            };
        fetch('https://damp-cliffs-96306.herokuapp.com/crypto', requestOptions)
            .then(response => response.json())
            .then(result => this.props.reloadData())
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }

  render() {
    return (
    <div className="container col-sm-4">
      <h3>Add crypto to portfolio</h3>
      <form onSubmit={this.mySubmitHandler}>
        <label htmlFor='cryptoType'>Pick crypto currency</label>
        <select className='form-control' name='cryptoType' onChange={this.myChangeHandler}>
          <option value="BITCOIN">Bitcoin</option>
          <option value="ETHEREUM">Ethereum</option>
          <option value="RIPPLE">Ripple</option>
        </select>
        <label htmlFor='amount' >Enter amount:</label>
        <input
            className='form-control'

            type='number'
            name='amount'
            required
            onChange={this.myChangeHandler}
        />
        <label htmlFor='walletLocation' >Enter wallet location:</label>
        <input
            className='form-control'
            type='text'
            name='walletLocation'
            required
            onChange={this.myChangeHandler}
        />
        <p>Enter date of purchase:</p>
        <input
            className='form-control'
            type='date'
            name='purchaseDate'
            defaultValue={this.state.purchaseDate}
            onChange={this.myChangeHandler}
        />
        <p />
        <input className='btn btn-primary' type='submit' />
      </form>
    </div>
    );
  }
}

export default Form;