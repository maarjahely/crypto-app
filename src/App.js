import React from 'react';
import './App.css';
import AddCrypto from './AddCrypto'

function App() {
  return (
    <div className="App">
        <AddCrypto />
    </div>
  );
}

export default App;
